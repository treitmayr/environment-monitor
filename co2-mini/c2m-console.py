#!/usr/bin/env python2

import sys, fcntl, time, datetime

shuffle = [2, 4, 0, 7, 1, 6, 5, 3]
cstate = [0x48,  0x74,  0x65,  0x6D,  0x70,  0x39,  0x39,  0x65]
ctmp = [((cstate[i] >> 4) | (cstate[i]<<4)) & 0xff for i in range(8)]


def decrypt(key, data):
    phase1 = [data[i] for i in shuffle]
    phase2 = [phase1[i] ^ key[i] for i in range(8)]
    phase3 = [((phase2[i] >> 3) | (phase2[(i-1+8)%8] << 5)) & 0xff for i in range(8)]
    out = [(0x100 + phase3[i] - ctmp[i]) & 0xff for i in range(8)]
    return out


if __name__ == "__main__":
    # Key retrieved from /dev/random, guaranteed to be random ;)
    key = [0xc4, 0xc6, 0xc0, 0x92, 0x40, 0x23, 0xdc, 0x96]
    
    fp = open(sys.argv[1], "a+b",  0)
    
    HIDIOCSFEATURE_9 = 0xC0094806
    set_report = "\x00" + "".join(chr(e) for e in key)
    fcntl.ioctl(fp, HIDIOCSFEATURE_9, set_report)
    
    values = {}
    
    while True:
        data = list(ord(e) for e in fp.read(8))
        decrypted = decrypt(key, data)
        if decrypted[4] != 0x0d or (sum(decrypted[:3]) & 0xff) != decrypted[3]:
            print hd(data), " => ", hd(decrypted),  "Checksum error"
        else:
            op = decrypted[0]
            val = decrypted[1] << 8 | decrypted[2]
            
            values[op] = val
            
            if 0x50 in values and 0x42 in values:
                print "%s\t%2.2f\t%4i" % (datetime.datetime.now().isoformat(), values[0x42]/16.0-273.15, values[0x50])
                values = {}

